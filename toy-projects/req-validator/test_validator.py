import unittest

from mock import Mock
from rest_framework.response import Response

from validator import isdigit, istrue
from validator import validate, record, ValidationError


class TestValidator(unittest.TestCase):

    def get_request(self, method='GET', params={}):
        request = Mock()
        request.method = method
        request.QUERY_PARAMS = params
        return request

    def test_required_implicit(self):
        class Foo:
            @validate((
                record('username'),
            ))
            def get(self, request, *args, **kwargs):
                return Response({}, status=200)

        f = Foo()
        req = self.get_request(params=dict(username='foo'))
        ret = f.get(req)
        self.assertEqual(ret.status_code, 200)

        req = self.get_request(params=dict())
        ret = f.get(req)
        self.assertEqual(ret.status_code, 400)

    def test_required_false(self):
        class Foo:
            @validate((
                record('username', required=False),
            ))
            def get(self, request, *args, **kwargs):
                return Response({}, status=200)

        f = Foo()
        req = self.get_request(params=dict(username='foo'))
        ret = f.get(req)
        self.assertEqual(ret.status_code, 200)

        req = self.get_request(params=dict())
        ret = f.get(req)
        self.assertEqual(ret.status_code, 200)

    def test_empty_value(self):
        class Foo:
            @validate((
                record('username', required=False),
            ))
            def get(self, request, *args, **kwargs):
                return Response({}, status=200)

        f = Foo()
        req = self.get_request(params=dict(username=''))
        ret = f.get(req)
        self.assertEqual(ret.status_code, 400)

        req = self.get_request(params=dict(username='    '))
        ret = f.get(req)
        self.assertEqual(ret.status_code, 400)

    def test_validator(self):
        class Foo:
            @validate((
                record('username', validator=isdigit),
            ))
            def get(self, request, *args, **kwargs):
                return Response({}, status=200)

        f = Foo()
        req = self.get_request(params=dict(username='1'))
        ret = f.get(req)
        self.assertEqual(ret.status_code, 200)

        req = self.get_request(params=dict(username='foo'))
        ret = f.get(req)
        self.assertEqual(ret.status_code, 400)

    def test_validator_custom_ret(self):
        def validator(value):
            assert value == 'foo', repr(value)
            raise ValidationError(500, 'apple')

        class Foo:
            @validate((
                record('username', validator=validator),
            ))
            def get(self, request, *args, **kwargs):
                return Response({}, status=200)

        f = Foo()
        req = self.get_request(params=dict(username='foo'))
        ret = f.get(req)
        self.assertEqual(ret.status_code, 500)
        self.assertEqual(ret.data, {'username' : 'apple'})

    def test_validators(self):
        self.assertTrue(isdigit('1'))
        self.assertFalse(isdigit('foo'))
        self.assertTrue(istrue('foo'))
        self.assertFalse(istrue(''))
