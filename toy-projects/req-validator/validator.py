# -*- coding: UTF-8 -*-

"""
Validatore per i parametri passati da request.
Esempio di utilizzo:

    from validator import validate, record, isdigit

    class StationLogin(APIView):

        @validate((
            record('username'),
            record('password', validator=isdigit),
            record('ip_address'),
            record('description', required=False),
        ))
        def put(self, request, *args, **kwargs):
            ...


:validator: una callable che se ritorna False fa fallire la validazione
            può anche lanciare ValidationError per ottenere un
            (http_code, msg) customizzato.
:required: True di default, assume che l'argomento sia presente, altrimenti
           torna (400, 'Missing argument'
"""

import collections
from rest_framework.response import Response


class ValidationError(Exception):
    """Raise this from a custom validator in order to return a custom
    error code and message.
    """
    def __init__(self, code, msg):
        self.code = code
        self.msg = msg


class record(collections.namedtuple('record', ['name', 'required', 'validator'])):
    def __new__(cls, name, required=True, validator=None):
        return super(record, cls).__new__(cls, name, required, validator)


# ...alcuni validatori
isdigit = lambda x: x.isdigit()
istrue = lambda x: bool(x)


class validate(object):
    """validate decorator."""

    def __init__(self, records):
        for x in records:
            assert isinstance(x, record), repr(records)
        self.records = records

    def __call__(self, original_func):
        def wrapper(*args, **kwargs):
            inst = args[0]  # the method's class instance
            request = args[1]
            records = decorator_self.records
            if request.method == 'GET':
                qdict = request.QUERY_PARAMS
            elif request.method == 'PUT':
                qdict = request.DATA
            else:
                raise NotImplementedError("don't know how to handle {}".format(
                    request.method))
            error_codes = set()
            errors = {}
            for record in records:
                name = record.name
                if record.required and name not in qdict:
                    errors[name] = "Missing value"
                    error_codes.add(400)
                    continue
                #
                if name in qdict:
                    param_value = qdict[name]
                    if isinstance(param_value, basestring):
                        if not param_value.strip():
                            errors[name] = "Empty value"
                            error_codes.add(400)
                            continue
                    #
                    if record.validator is not None:
                        code = 400
                        msg = "Invalid value"
                        try:
                            ok = record.validator(param_value)
                        except ValidationError as err:
                            ok = False
                            code = err.code
                            msg = err.msg
                        if not ok:
                            errors[name] = msg
                            error_codes.add(code)
                            continue
            if errors:
                return Response(errors, status=error_codes.pop())
            else:
                return original_func(*args,**kwargs)

        decorator_self = self
        return wrapper