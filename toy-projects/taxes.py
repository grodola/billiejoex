from __future__ import division

# --- config
mensile = 9500
mesi_lavorati = 5
lordo = mensile * mesi_lavorati
affitto = 600 * 12
locus_costo_mensile = 140
locus_num_mesi = 7


def percent_of(total, percent):
    return (total / 100 * percent)


def percent_diff(a, b):
    return round(100 - (a / b * 100), 1)


assert lordo >= 40000, lordo
first = 40000
second = lordo - 40000
netto = lordo
tasse = percent_of(first, 15) + percent_of(second, 36)
netto -= tasse

# con spese
spese = 0
spese += locus_costo_mensile * locus_num_mesi
spese += affitto
netto -= spese

minus_percent = percent_diff(netto, lordo)
per_month_full_year = int(netto / 11)
per_month_single_month = int(netto / mesi_lavorati)
netto_in_italia = lordo - percent_of(lordo, 53)
diff_italia = int(netto - netto_in_italia)

print("""\
lordo                        {lordo}
mesi lavorati                {mesi_lavorati}
spese anno                   {spese}
affitto                      {affitto}
netto                        {netto}
tasse                        {tasse}
percentuale (totale)         -{minus_percent}%
al mese (su 12)              {per_month_full_year}
al mese (su {mesi_lavorati})               {per_month_single_month}
diff italia                  +{diff_italia}
""".format(**locals()))
