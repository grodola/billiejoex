#/usr/bin/env python3.3

"""
A dummy FTP server based on Tulip:
https://code.google.com/p/tulip/
"""

import socket
import os

import tulip
from tulip import futures


BUFSIZE = 65536


class FTPServer(tulip.Protocol):

    # --- tulip overridden callbacks

    def connection_made(self, transport):
        self._closed = False
        self.client_addr = transport._sock.getpeername()
        self.log("connection from {}".format(self.client_addr))
        self.transport = transport
        self.data_sock = None
        self.respond('220 welcome')

    def data_received(self, data):
        # TODO replace this with StreamReader (after I figure out how to
        # use it)
        line = data.decode('utf8').strip()
        self.log('<- ' + line)
        cmd = line.split(' ')[0].lower()
        space = line.find(' ')
        if space != -1:
            arg = line[space + 1:]
        else:
            arg = ""
        if hasattr(self, 'cmd_' + cmd):
            method = getattr(self, 'cmd_' + cmd)
            method(arg)
        else:
            self.respond('550 command "%s" not understood.' % cmd)

    def connection_lost(self, exc):
        self.log("connection lost")
        if exc:
            pass
            #import ipdb; ipdb.set_trace()
        self.close()

    def eof_received(self):
        self.log("eof received")
        self.close()

    # --- utility methods

    def log(self, msg):
        print("{} {}".format(self.client_addr, msg))

    def respond(self, line):
        self.log('-> ' + line)
        if isinstance(line, str):
            line = line.encode('utf8')
        self.transport.write(line + b'\r\n')

    def transfer_complete(self):
        self.data_sock.close()
        self.data_sock = None
        self.respond('226 transfer complete')

    def transfer_failed(self):
        self.data_sock.close()
        self.data_sock = None
        self.respond('426 data conn closed')

    def close(self):
        if not self._closed:
            self._closed = True
            if self.data_sock is not None:
                self.data_sock.close()
                self.data_sock = None
            self.transport.close()
            # XXX it appears self.transport.close() does not close the
            # socket (should it?)
            self.transport._sock.close()
            self.transport = None

    # --- protocol implementation

    def cmd_user(self, arg):
        self.respond('331 username ok')

    def cmd_pass(self, arg):
        self.respond('230 password ok')

    def cmd_type(self, arg):
        self.respond('200 type ok')

    def cmd_dele(self, arg):
        try:
            os.remove(arg)
        except OSError as err:
            self.respond('550 ' + err.strerror)
        else:
            self.respond('250 dele ok')

    def cmd_quit(self, arg):
        self.respond('221 quit ok')
        self.close()

    @tulip.task
    def cmd_pasv(self, arg):
        sock = socket.socket()
        sock.setblocking(False)
        sock.bind((self.transport._sock.getsockname()[0], 0))
        sock.listen(1)
        ip, port = sock.getsockname()[:2]
        ip = ip.replace('.', ',')
        p1 = port / 256
        p2 = port % 256
        self.respond('227 entering passive mode (%s,%d,%d)' % (ip, p1, p2))
        self.data_sock, _ = yield from loop.sock_accept(sock)

    @tulip.task
    def cmd_port(self, arg):
        addr = list(map(int, arg.split(',')))
        ip = '%d.%d.%d.%d' % tuple(addr[:4])
        port = (addr[4] * 256) + addr[5]
        sock = socket.socket()
        yield from loop.sock_connect(sock, (ip, port))
        self.data_sock = sock
        self.respond('200 active data connection established')

    @tulip.task
    def cmd_stor(self, arg):
        if self.data_sock is None:
            return self.respond('500 issue pasv first')
        self.respond('125 stor ok')

        with open(arg, 'wb') as file:
            while True:
                #chunk = self.data_sock.recv(BUFSIZE)  # blocking version
                try:
                    chunk = yield from loop.sock_recv(self.data_sock, BUFSIZE)
                except ConnectionResetError:
                    return self.transfer_failed()
                else:
                    if not chunk:
                        break
                    file.write(chunk)
        self.log('whole file was sent')
        self.transfer_complete()

    @tulip.task
    def cmd_retr(self, arg):
        if self.data_sock is None:
            return self.respond('500 issue pasv first')
        self.respond('125 retr ok')

        with open(arg, 'rb') as file:
            while True:
                chunk = file.read(BUFSIZE)
                if not chunk:
                    break
                try:
                    yield from loop.sock_sendall(self.data_sock, chunk)
                except ConnectionResetError:
                    return self.transfer_failed()
                #self.data_sock.sendall(chunk)  # blocking version
        self.log('whole file was sent')
        self.transfer_complete()


def main():
    global loop
    loop = tulip.get_event_loop()
    loop.set_log_level(futures.STACK_DEBUG)
    task = loop.start_serving(FTPServer, host='localhost', port=2021)
    socks = loop.run_until_complete(task)
    print('serving on', socks[0].getsockname())
    loop.run_forever()

if __name__ == '__main__':
    main()
