#!/usr/bin/env python

"""
Usage:
    install-init-script.py <appname> <modname> <activate-script> [-i|--install]

About:
    Install this application as an init script which is run on system
    startup. Debian-like and RedHat-like systems are supported.

    <appname> is the name which is given to the init file.
    Later it will be used to reference it via "sudo service {appname} start".

    <modname> is the module to invoke on start/stop/restart as:
    $ "python -m {modname} start|stop|restart".
    As such the Python app must provide:
    - a __main__.py file which defines how to start the app.
    - support start|stop|restart|status|reopen_transcript
      command line arguments.

    <activate-script> is the path to virtualenv 'activate' script.

Examples:
    install-init-script.py monit monit .venv/bin/activate     # print out file
    install-init-script.py monit monit .venv/bin/activate -i  # install
"""

import os
import platform
import subprocess
import sys
import tempfile
import textwrap

import docopt

# static constants
HERE = os.path.abspath(os.path.dirname(__file__))
TMP_DIR = tempfile.mkdtemp()
DEBIAN_LIKE = 0
REDHAT_LIKE = 1
PY3 = sys.version_info >= (3, 0)
TEMPLATE = """\
#!/bin/sh

set -e

{{{header}}}

. {{{init_functions}}}

APPNAME={{{appname}}}
MODNAME={{{modname}}}

. {{{activate_script}}}

case "$1" in
    start)
        python -m $MODNAME start
        ;;
    stop)
        python -m $MODNAME stop
        ;;
    restart|reload|force-reload)
        python -m $MODNAME restart
        ;;
    status)
        python -m $MODNAME status
        ;;
    reopen_transcript)
        python -m $MODNAME reopen_transcript
        ;;
    *)
        echo "{{{appname}}} start|stop|restart|status|reopen_transcript"
        exit 1
        ;;
esac
exit $?
"""


def am_i_root():
    """Return True if running as root."""
    return os.geteuid() == 0


def which(program):
    """Same as UNIX which command. Return None on command not found."""
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None


def sh(cmdline):
    """Run cmd in a subprocess and print its output."""
    print(hilite("$ %s" % cmdline, ok=None, bold=1))
    out = subprocess.check_output(cmdline, shell=True)
    if PY3 and isinstance(out, bytes):
        out = out.decode()
    for line in out.split('\n'):
        print("  " + line)
    return out


def sh_as_root(cmdline):
    """Same as above but running as root."""
    assert isinstance(cmdline, str)
    if not am_i_root():
        if which('sudo'):
            cmdline = "sudo " + cmdline
        else:
            assert "'" not in cmdline, cmdline
            cmdline = "su - root -c '%s'" % cmdline
    return sh(cmdline)


def hilite(string, ok=True, bold=False):
    """Return an highlighted version of 'string'."""
    attr = []
    if ok is None:  # no color
        pass
    elif ok:   # green
        attr.append('32')
    else:   # red
        attr.append('31')
    if bold:
        attr.append('1')
    return '\x1b[%sm%s\x1b[0m' % (';'.join(attr), string)


def get_platform():
    def is_debian_like():
        return (os.path.exists("/etc/init.d/")
                and which("update-rc.d")
                and which('apt-get'))

    def is_redhat_like():
        return (os.path.exists("/etc/init.d/")
                and which("chkconfig")
                and which("update-rc.d")
                and which('yum'))

    if is_debian_like():
        return DEBIAN_LIKE
    elif is_redhat_like():
        return REDHAT_LIKE
    else:
        raise SystemExit('platform not supported %r' % platform.platform())


def install(appname, modname, activate_script, install=False):
    assert appname, appname
    if not os.path.isabs(activate_script):
        activate_script = os.path.realpath(activate_script)
    assert os.path.exists(activate_script), activate_script

    # make sure module can be imported
    subprocess.call(["python", "-c", "import %s" % modname])

    platf = get_platform()
    if platf == DEBIAN_LIKE:
        header = textwrap.dedent("""\
            ### BEGIN INIT INFO
            # Provides:          skeleton
            # Required-Start:    $remote_fs $syslog
            # Required-Stop:     $remote_fs $syslog
            # Default-Start:     2 3 4 5
            # Default-Stop:      0 1 6
            # Short-Description: m3 init script
            # Description:       m3 init script
            ### END INIT INFO
            """)
    elif platf == REDHAT_LIKE:
        header = textwrap.dedent("""\
            # Startup script for the m3 application
            # chkconfig: - 99 01
            # description: m3 init script
            """)
    else:
        assert 0

    text = TEMPLATE
    text = text.replace("{{{header}}}", header)
    text = text.replace("{{{appname}}}", appname)
    text = text.replace("{{{modname}}}", modname)
    text = text.replace("{{{activate_script}}}", activate_script)
    if os.path.exists("/etc/init.d/functions"):
        text = text.replace("{{{init_functions}}}", "/etc/init.d/functions")
    elif os.path.exists("/lib/lsb/init-functions"):
        text = text.replace("{{{init_functions}}}", "/lib/lsb/init-functions")
    else:
        raise ValueError("can't find init functions")

    if not install:
        print(text)
    else:
        outfile = os.path.join(TMP_DIR, appname)
        with open(outfile, 'w') as f:
            f.write(text)
        if platf == DEBIAN_LIKE:
            if os.path.isfile('/etc/init.d/%s' % appname):
                sh_as_root("update-rc.d -f %s remove" % appname)
            sh_as_root("mv %s /etc/init.d" % outfile)
            sh_as_root("chmod +x /etc/init.d/%s" % appname)
            sh_as_root("update-rc.d %s defaults" % appname)
        elif platf == REDHAT_LIKE:
            if os.path.isfile('/etc/init.d/%s' % appname):
                sh_as_root("chkconfig --del %s" % appname)
            sh_as_root("mv %s /etc/init.d" % outfile)
            sh_as_root("chmod +x /etc/init.d/%s" % appname)
            sh_as_root("chkconfig --add %s" % appname)
            sh_as_root("chkconfig --level 2345 %s on" % appname)
        else:
            assert 0

        msg = "Init script was successfully installed! "
        if platf == DEBIAN_LIKE:
            if which('sudo') and not am_i_root():
                cmd = 'sudo service %s status' % appname
            else:
                cmd = 'service %s status' % appname
            msg += "\nTry running %r to check it works." % cmd
        print(hilite(msg, bold=1))


def main():
    args = docopt.docopt(__doc__)
    return install(args['<appname>'],
                   args['<modname>'],
                   args['<activate-script>'],
                   install=args['-i'])

if __name__ == '__main__':
    try:
        main()
    except SystemExit as err:
        sys.exit(hilite(str(err), ok=False))
