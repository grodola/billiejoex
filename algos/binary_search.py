def binary_search(data, value):
    left = 0
    right = len(data) - 1
    while left <= right:
        middle = (left + right) // 2
        if value < data[middle]:
            right = middle - 1
        elif value > data[middle]:
            left = middle + 1
        else:
            return middle
    raise ValueError('Value is not in the list')


data = [1, 2, 3, 4, 5, 6, 7, 8, 9]
print(binary_search(data, 8))


def binary_search(data, value):
    left = 0
    right = len(data) - 1
    while True:
        middle = (left + right) // 2
        if value < data[middle]:
            right = middle - 1
        elif value > data[middle]:
            left = middle + 1
        else:
            return middle


data = [1, 2, 3, 4, 5, 6, 7, 8, 9]
print(binary_search(data, 8))
