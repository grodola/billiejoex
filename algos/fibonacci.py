# def fib():
#     """
#     >>> f = fib()
#     >>> for x in range(10):
#     ...     print f.next()
#     1
#     1
#     2
#     3
#     5
#     8
#     13
#     21
#     34
#     55
#     """
#     a = 0
#     b = 1
#     while True:
#         yield a
#         a, b = b, a + b

# def fib_from_n(n):
#     """Utility function (basically useless)."""
#     gen = fib()
#     for x in xrange(n - 1):
#         next(gen)
#     return next(gen)


def fib():
    a = 0
    b = 1
    while True:
        yield a
        a, b = b, a + b


gen = fib()
for x in range(20)  :
    print(next(gen))
