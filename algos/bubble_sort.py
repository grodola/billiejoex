# Velocita: O(N2)


def bsort(seq):
    while True:
        did_swap = False
        for i in range(len(seq)):
            curr = seq[i]
            try:
                next = seq[i + 1]
            except IndexError:  # EOL
                break
            else:
                if curr > next:
                    # swap them
                    seq[i + 1] = curr
                    seq[i] = next
                    did_swap = True
        if not did_swap:
            break
    return seq


print(bsort([12, 2, 4, 6, 13, 1, 5]))
