# Velocita: O(n log n)

def qsort(seq):
    if seq == []:
        return []
    pivot = seq[0]
    lesser = [x for x in seq[1:] if x < pivot]
    greater = [x for x in seq[1:] if x >= pivot]
    return qsort(lesser) + [pivot] + qsort(greater)


print(qsort([12, 2, 4, 6, 13, 1, 5]))
