See:
* https://towardsdatascience.com/understanding-time-complexity-with-python-examples-2bda6e8158a7
* http://www.perlmonks.org/?node_id=227909

Sorted by speed:

╔══════════════════╦═════════════════╦═════════════════════════════════════════════════╗
║       Name       ║ Time Complexity ║                   Examples                      ║
╠══════════════════╬═════════════════╬═════════════════════════════════════════════════╣
║ Constant Time    ║       O(1)      ║ x in dict                                       ║
╠══════════════════╬═════════════════╬═════════════════════════════════════════════════╣
║ Logarithmic Time ║     O(log n)    ║ search binary-tree, phone book                  ║
╠══════════════════╬═════════════════╬═════════════════════════════════════════════════╣
║ Linear Time      ║       O(n)      ║ x in list                                       ║
╠══════════════════╬═════════════════╬═════════════════════════════════════════════════╣
║ Quasilinear Time ║    O(n log n)   ║ quick-sort, merge-sort                          ║
╠══════════════════╬═════════════════╬═════════════════════════════════════════════════╣
║ Quadratic Time   ║      O(n^2)     ║ bubble-sort                                     ║
╠══════════════════╬═════════════════╬═════════════════════════════════════════════════╣
║ Exponential Time ║      O(2^n)     ║                                                 ║
╠══════════════════╬═════════════════╬═════════════════════════════════════════════════╣
║ Factorial Time   ║       O(n!)     ║                                                 ║
╚══════════════════╩═════════════════╩═════════════════════════════════════════════════╝

O(1) (constant time)
====================

Regardless of input size, time is the same. Examples:

    x in dict

    def is_first_element_none(seq):
        return seq[0] == None


O(log n) (logarithmic time)
===========================

* E.g. a binary tree.
* E.g. a phone book, where names are sorted alphabetically: you divide the
  input at each loop, and know whether to search left or right.
* size of the input is halved at each step
* Doubling the lenght of the data set has a relatively small effect on
  performaces.
* Time grows slowly.
* If prcessing 10 elements takes 1 sec, for 100 elements it will take 2 secs,
  for 1000 it will take 2 etc.

```
def binary_search(data, value):
    n = len(data)
    left = 0
    right = n - 1
    while left <= right:
        middle = (left + right) // 2
        if value < data[middle]:
            right = middle - 1
        elif value > data[middle]:
            left = middle + 1
        else:
            return middle
    raise ValueError('Value is not in the list')

data = [1, 2, 3, 4, 5, 6, 7, 8, 9]
print(binary_search(data, 8))
```

O(n) (linear time)
==================

The algorithm's performance is directly proportional to the size of the data
set being processed. Example:

    x in list

O(n log n) (quasi linear time)
=============================

* See: https://towardsdatascience.com/understanding-time-complexity-with-python-examples-2bda6e8158a7
* when each operation in the input data have a logarithm time complexity
* used by most sorting algorithms
* examples:

    # mergesort, timsort, heapsort

O(n^2) (quadratic time)
=======================

The algorithm's performance is proportional to the square of the data set size.
This happens when the algorithm processes each element of a set, and that
processing requires another pass through the set. Example:

    # bubble sort algo

    for x in data:
        for y in data:
            print(x, y)

...O(N3), O(N4), O(N5), O(N6), etc. are what you would expect. Lots of inner
loops.

O(2^n)
======

O(2N) means you have an algorithm with exponential time (or space, if someone
says space) behavior. In the 2 case, time or space double for each new element
in data set. There's also O(10N), etc. In practice, you don't need to worry
about scalability with exponential algorithms, since you can't scale very far
unless you have a very big hardware budget.
